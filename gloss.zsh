# Audiokraft
alias eko="echo $1"

COMPONENTS_DIR="/Library/Audio/Plug-Ins/Components/"

aucp () {
  command sudo cp -Rpv $1 $COMPONENTS_DIR
}

alias opau="open $COMPONENTS_DIR"
