merges-introducing() {
  git rev-list --ancestry-path --parents --reverse $1...$2 \
  | awk '{seen[$1]=1} NR>1 && !seen[$2] {print $1}' \
  | xargs git show --oneline --no-patch
}

merges-introducing2() {
  local introducing;
  if introducing=`git rev-parse $1`; then 
    shift
    git rev-list --ancestry-path --parents --reverse ^$introducing ${@-HEAD} \
    | awk '{seen[$1]=1} NR>1 && !seen[$2] {print $1}' \
    | xargs git show --oneline --no-patch
   fi
}

git-author(){
  git log --author="$1" --pretty=tformat: --numstat \
  | gawk '{ add += $1 ; subs += $2 ; loc += $1 - $2 } END { printf "added lines: %s removed lines : %s total lines: %s\n",add,subs,loc }' -
}

git-del-remote-merged(){
  git branch -r --merged | grep -v master | sed 's/origin\///' | xargs -n 1 git push --delete origin
}

git() {
    if [[ $@ == "pug" ]]; then
        command pugme
    else
        command git "$@"
    fi
}
