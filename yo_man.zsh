init() {
  local GITHUB_USERNAME=jameswomack
  local AUTHOR_FULLNAME="James J. Womack"

  mkcd $1
  git init
  
  echo "{
  \"name\": \"$1\",
  \"version\": \"0.0.0\",
  \"description\": \"$2\",
  \"keywords\": [],
  \"main\": \"index.js\",
  \"repository\": {
    \"type\": \"git\",
    \"url\": \"http://github.com/$GITHUB_USERNAME/$1\"
  },
  \"scripts\": {
    \"test\": \"echo \\\"Error: no test specified\\\" && exit 1\"
  },
  \"author\": \"$AUTHOR_FULLNAME\",
  \"license\": \"ISC\"
}" > package.json

  echo "# $1\n## $2\n\`Hello World\`" > README.md

  git remote add origin http://github.com/$GITHUB_USERNAME/$1.git

  echo "Created git repo $1 with description $2"
}

mkcd() {
  # && isn't used to prevent 
  # cd from not getting fired
  # when mkdir errors out
  mkdir -p $1
  cd $1
}
