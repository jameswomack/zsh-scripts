#
# These are variables that need to be present for everything
#


# Tersity en masse
alias c=cd

# src dirs
export PROJ_DIR=~/Projects
export GHUB_DIR=$PROJ_DIR/github
export GHUB_JW_DIR=$GHUB_DIR/jameswomack
export STASH_DIR=$PROJ_DIR/stash
export STASH_CA_DIR=$STASH_DIR/ca
export STASH_BHNODE_DIR=$STASH_DIR/bhnode
export BITB_DIR=$PROJ_DIR/bitbucket
export BITB_JW_DIR=$BITB_DIR/jameswomack
export ZSH_SCRIPTS_DIR=$BITB_JW_DIR/zsh-scripts
