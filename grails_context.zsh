PWD="$(pwd)"
bName=`eval basename $PWD`
GRAILS_DEBUG_PORT=0

if [[ "$bName" == "tagging-ui" ]]; then
  GRAILS_DEBUG_PORT=9013
elif [[ "$bName" == "beehive-ui" ]]; then
  GRAILS_DEBUG_PORT=9012
elif [[ "$bName" == "beehive-vendor-portal" ]]; then
  GRAILS_DEBUG_PORT=9011
elif [[ "$bName" == "astroboy-ui" ]]; then
  GRAILS_DEBUG_PORT=9010
elif [[ "$bName" == "snapbox-ui" ]]; then
  GRAILS_DEBUG_PORT=9009
else
  GRAILS_DEBUG_PORT=9008
fi

alias gsuirun="gradlew --stacktrace -Pstatus=snapshot clean grails-compile buildDeb"
alias gduirun="gradlew --stacktrace -DdependencyHeaven.enabled=true clean downloadLock lockDependencies buildDeb"
