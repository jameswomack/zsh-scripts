#!/bin/zsh

local SOURCERER_PATH="$0"

for file in $ZSH_SCRIPTS_DIR/*.zsh; do
  if [[ "$SOURCERER_PATH" != "$file" ]]; then
    source $file
  fi
done
