source $ZSH_SCRIPTS_DIR/bootstrap.zsh

alias nbl="c ~/Code/noble"
alias gpom="git push origin master"

alias zscripts="c $ZSH_SCRIPTS_DIR"

# Personal / OSS
alias ghub="c $GHUB_DIR"
alias bitb="c $BITB_DIR"
alias ghjw="c $GHUB_DIR/jameswomack"
alias exti="c $GHUB_DIR/jameswomack/exploding-video-tiles"

# Netflix
alias stash="c $STASH_DIR"

# Netflix DEA
alias ignite="c $STASH_DIR/dseplat/ignite"
alias nmdash="c $STASH_DIR/nmdash/nmdash"
alias algodash="c $STASH_DIR/ALDA/algodash"

# Netflix DPA DX
alias abacuse="c $GHUB_DIR/netflix/abacus/packages/abacus-editor"
alias abacus="c $GHUB_DIR/netflix/abacus/packages/abacus"
alias abacusea="c $GHUB_DIR/netflix/abacus-editor-app"
alias abacusa="c $GHUB_DIR/netflix/abacus-app-template"

# Chrome
export chromecanary="/Applications/Google Chrome Canary.app/Contents/MacOS/Google Chrome Canary"

# General
alias gt="git"
alias kat="cat -n "
alias ssh-add="ssh-add -K "
alias reload=". ~/.zshrc && echo 'ZSH config reloaded from ~/.zshrc'"
alias zshrc="vim ~/.zshrc && reload"
alias rn="rm"

# alias denv="eval \"$(docker-machine env default)\""
#alias cv="echo \"CHROME\n$chromecanary\nv$($chromecanary --version | sed -r 's/(Google Chrome |canary)//g;') \""
#alias nv="cv; echo \"\" && echo \"NODE\"; which node && node -v && echo \"\nNPM\"; which npm && echo v$(npm -v)"
# alias code="open $1 -a \"/Applications/Visual Studio Code.app\""
alias coder="open $@ -a \"/Applications/Visual Studio Code - Insiders.app\""

# DRYs up correcting execution permissions for the raw MacOS Electron executable, a MacOS Sierra issue
function sierraExecFix () {
  chmod +x "$1/Contents/MacOS/Electron"
}

# DRYs up opening n-number files in an app
function opap () {
  app=${@: -1}
  open ${@:1:$#-1} -a "$app" || sierraExecFix $app && open ${@:1:$#-1} -a "$app"
}

# DRYs up warning an app is being opened from the Downloads folder
function wrnlo () {
  echo "$1 is at $2, consider moving it to $3"
}

# This is the main script that you'll use day-to-day.
# Prefers Insider edition over stable & /Applications over ~/Downloads
code () {
  name="Visual Studio Code"
  namei="$name - Insiders"
  apps="/Applications/$name"
  appsi="/Applications/$namei"
  dls="~/Downloads/$name"
  dlsi="~/Downloads/$namei"
  vsc="$apps.app"
  vsci="$appsi.app"
  vscd="$dls.app"
  vscd="$dlsi.app"
  if [[ -d $vsci ]]
  then
    opap $@ $vsci
  elif [[ -d $vsc ]]
  then
    opap $@ $vsc
  elif [[ -d $vscd ]]
  then
    wrnlo $name $vscd $vsc
    opap $@ $vscd
  elif [[ -d $vscdi ]]
  then
    wrnlo $namei $vscdi $vsci
    opap $@ $vscdi
  else
    echo "$name not found"
  fi
}
