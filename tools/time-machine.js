#!/usr/bin/env node

var FS     = require('fs'),
    Moment = require('moment'),
    yargs  = require('yargs')

function getHome () {
  return process.env[process.platform === 'win32' ? 'USERPROFILE' : 'HOME']
}

var argv = yargs
  .options({
    '~': {
      alias    : 'home',
      describe : 'What to treat as your ~',
      type     : 'string',
      nargs    : 1,
      demand   : 'Home is required',
      default  : getHome()
    },
    f  : {
      alias    : 'files',
      describe : 'Files that were backed up',
      type     : 'array',
      demand   : 'Files are required',
      default  : ['.zshrc', 'zsh_history']
    }
  })
  .help('h')
  .usage('Usage : $0 -~ [home path] -f [file paths]')
  .argv

FS.readdir(argv.home, function (error, fileList) {
  if (error){
    return console.error(error)
  }

  var SPLIT_ON = '_'

  var keyMap = argv.files.reduce(function (context, argvFilename) {
    context[argvFilename] = argvFilename

    return context
  }, {})

  function isBackup (filename) {
    var containsName = argv.files.some(function (argvFilename) {
      return !!~filename.indexOf(argvFilename + SPLIT_ON)
    })

    console.log(containsName, !(filename in keyMap), filename)

    return containsName && !(filename in keyMap)
  }

  function TMFile (filename) {
    if (!(this instanceof TMFile)){
      return new TMFile(filename)
    }

    this.filename = filename
  }

  Object.defineProperties(TMFile.prototype, {
    backupDate : {
      get : function () {
        var split = this.filename.split(SPLIT_ON)
        var dateString = split[split.length-1]

        return Moment(dateString, 'YYYYMMDDHHmmss').format('LLLL')
      },

      enumerable : true
    },

    baseName : {
      get : function () {
        var split = this.filename.split(SPLIT_ON)
        return split.slice(0, split.length-1).join(SPLIT_ON)
      },

      enumerable : true
    }
  })

  var tmFileMap = fileList.filter(isBackup).map(TMFile).reduce(function (context, tmFile) {
    context[tmFile.baseName] || (context[tmFile.baseName] = [])
    context[tmFile.baseName].push(tmFile)
    return context
  }, {})

  Object.keys(tmFileMap).forEach(function (tmFileBaseName) {
    console.info(tmFileBaseName + '\n=====================\n')
    tmFileMap[tmFileBaseName].forEach(function (tmFile) {
      console.info('Date for ' + tmFile.filename + ': ' + tmFile.backupDate)
    })
  })
}, {})
